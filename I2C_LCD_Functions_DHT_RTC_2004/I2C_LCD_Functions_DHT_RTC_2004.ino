#include <stdio.h>
#include "DS1302.h"
#include "Wire.h"
#include "LiquidCrystal_I2C.h"
//#include <LCD_I2C.h> 
#include "DHT.h"
// set the DHT Pin
#define DHTPIN 8
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

const int kCePin   = 4;  // RST
const int kIoPin   = 3;  // DAT
const int kSclkPin = 2;  // CLK
 
// LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3,POSITIVE);
//LCD_I2C lcd(0x27); // Default address of most PCF8574 modules, change according
LiquidCrystal_I2C lcd(0x27, 20, 4);
DS1302 rtc(kCePin, kIoPin, kSclkPin);
 
String dayAsString(const Time::Day day) {
  switch (day) {
    case Time::kSunday: return "Sun";
    case Time::kMonday: return "Mon";
    case Time::kTuesday: return "Tue";
    case Time::kWednesday: return "Wed";
    case Time::kThursday: return "Thur";
    case Time::kFriday: return "Fri";
    case Time::kSaturday: return "Sat";
  }
  return "(unknown day)";
}
void printTime() {
  Time t = rtc.time();
  const String day = dayAsString(t.day);
  char line1[50], line2[50];
  snprintf(line1, sizeof(line1), "%04d-%02d-%02d",
           t.yr, t.mon, t.date);
  snprintf(line2, sizeof(line2), "%02d:%02d:%02d %s  ",
           t.hr, t.min, t.sec,day.c_str() );

  lcd.setCursor(0,2);
  lcd.print(line1);
  lcd.setCursor(0,3);
  lcd.print(line2);
}

/* Arduino example code to display custom characters on I2C character LCD. More info: www.makerguides.com */

// Include the library:

// Create lcd object of class LiquidCrystal_I2C:
// LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2); // Change to (0x27,20,4) for 20x4 LCD.
// 5 x 8 dot character. A 0 means pixel off and a 1 means pixel on. The prefix ‘B’ is the binary formatter 0x Octave.
// Make custom characters:
byte Heart[] = {
  0x00,
  B01010,
  B11111,
  B11111,
  B01110,
  B00100,
  B00000,
  B00000
};

byte Bell[] = {
  B00100,
  B01110,
  B01110,
  B01110,
  B11111,
  B00000,
  B00100,
  B00000
};

byte Alien[] = {
  B11111,
  B10101,
  B11111,
  B11111,
  B01110,
  B01010,
  B11011,
  B00000
};

byte Check[] = {
  B00000,
  B00001,
  B00011,
  B10110,
  B11100,
  B01000,
  B00000,
  B00000
};

byte Speaker[] = {
  B00001,
  B00011,
  B01111,
  B01111,
  B01111,
  B00011,
  B00001,
  B00000
};

byte Sound[] = {
  B00001,
  B00011,
  B00101,
  B01001,
  B01001,
  B01011,
  B11011,
  B11000
};

byte Skull[] = {
  B00000,
  B01110,
  B10101,
  B11011,
  B01110,
  B01110,
  B00000,
  B00000
};

byte Lock[] = {
  B01110,
  B10001,
  B10001,
  B11111,
  B11011,
  B11011,
  B11111,
  B00000
};



void setup() {
  lcd.begin();
  lcd.backlight();
  delay(1000);
  rtc.halt(false);
  rtc.writeProtect(false);
  Serial.begin(9600);
  /* Time t(2021, 2, 20, 10, 31, 10, Time::kSaturday);
  rtc.time(t);
  */
 // Create new characters:
  lcd.createChar(0, Heart);
  lcd.createChar(1, Bell);
  lcd.createChar(2, Alien);
  lcd.createChar(3, Check);
  lcd.createChar(4, Speaker);
  lcd.createChar(5, Sound);
  lcd.createChar(6, Skull);
  lcd.createChar(7, Lock);

  // Clear the LCD screen:
  lcd.clear();

  // Print a message to the lcd:
  lcd.print("Custom Character");
  
}
 
void loop() {
  printTime();
  lcd.setCursor(0, 1);
  lcd.write(0);

  lcd.setCursor(2, 1);
  lcd.write(1);

  lcd.setCursor(4, 1);
  lcd.write(2);

  lcd.setCursor(6, 1);
  lcd.write(3);

  lcd.setCursor(8, 1);
  lcd.write(4);

  lcd.setCursor(10, 1);
  lcd.write(5);

  lcd.setCursor(12, 1);
  lcd.write(6);

  lcd.setCursor(14, 1);
  lcd.write(7);
  delay(3000);

/*============================================================================*/
   dht.begin();
  // read humidity
  float h = dht.readHumidity();
  //read temperature in Fahrenheit
  float f = dht.readTemperature(true);
  //read temperature in Celsius
  float c = dht.readTemperature();
  lcd.clear();
  lcd.setCursor(6, 0);
  lcd.print("% Humidity");
  lcd.setCursor(1, 0);
  lcd.print(h);
  delay(2500);
   for (int i = 0; i < 9; i++)
    {
        lcd.scrollDisplayRight();
        delay(300);
    }
     for (int i = 0; i < 9; i++)
    {
        lcd.scrollDisplayLeft();
        delay(300);
    } 
  delay(1000);
  lcd.setCursor(8, 1);
  lcd.print("Fahrenheit");
  lcd.setCursor(1, 1);
  lcd.print(f);
  //lcd.print(0xdf);
  lcd.print("\xDF");
  delay(1000);
  lcd.setCursor(8, 2);
  lcd.print("Celsius");
  lcd.setCursor(1, 2);
  lcd.print(c);
  lcd.print((char) 223);
  delay(5000);
   
    lcd.noAutoscroll();
    lcd.clear();
    delay(500);  
    
   


}
