#include <stdio.h>
#include "DS1302.h"
#include "Wire.h"
// #include "LiquidCrystal_I2C.h"
#include <LCD_I2C.h> 
#include "DHT.h"
// set the DHT Pin
#define DHTPIN 8
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

const int kCePin   = 4;  // RST
const int kIoPin   = 3;  // DAT
const int kSclkPin = 2;  // CLK
 
// LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3,POSITIVE);
LCD_I2C lcd(0x27); // Default address of most PCF8574 modules, change according

DS1302 rtc(kCePin, kIoPin, kSclkPin);
 
String dayAsString(const Time::Day day) {
  switch (day) {
    case Time::kSunday: return "Sun";
    case Time::kMonday: return "Mon";
    case Time::kTuesday: return "Tue";
    case Time::kWednesday: return "Wed";
    case Time::kThursday: return "Thur";
    case Time::kFriday: return "Fri";
    case Time::kSaturday: return "Sat";
  }
  return "(unknown day)";
}
void printTime() {
  Time t = rtc.time();
  const String day = dayAsString(t.day);
  char line1[50], line2[50];
  snprintf(line1, sizeof(line1), "%04d-%02d-%02d",
           t.yr, t.mon, t.date);
  snprintf(line2, sizeof(line2), "%02d:%02d:%02d %s  ",
           t.hr, t.min, t.sec,day.c_str() );
  lcd.setCursor(0,0);
  lcd.print(line1);
  lcd.setCursor(0,1);
  lcd.print(line2);
}

void setup() {
  lcd.begin();
  lcd.backlight();
  delay(1000);
  rtc.halt(false);
  rtc.writeProtect(false);
  Serial.begin(9600);
  /* Time t(2021, 2, 20, 10, 31, 10, Time::kSaturday);
  rtc.time(t);
  */
}
 
void loop() {
  printTime();
  delay(2000);

/*============================================================================*/
   dht.begin();
  // read humidity
  float h = dht.readHumidity();
  //read temperature in Fahrenheit
  float f = dht.readTemperature(true);
  //read temperature in Celsius
  float c = dht.readTemperature();
  lcd.clear();
  lcd.setCursor(6, 0);
  lcd.print("% Humidity");
  lcd.setCursor(1, 0);
  lcd.print(h);
  delay(2500);
   for (int i = 0; i < 11; i++)
    {
        lcd.scrollDisplayRight();
        delay(300);
    }
     for (int i = 0; i < 11; i++)
    {
        lcd.scrollDisplayLeft();
        delay(300);
    } 
  delay(1000);
  lcd.setCursor(6, 1);
  lcd.print(0xDF,"F");
  lcd.setCursor(1, 1);
  lcd.print(f);
  delay(1000);
  lcd.setCursor(13, 1);
  lcd.print("C");
  lcd.setCursor(8, 1);
  lcd.print(c);
  delay(5000);
   
    lcd.noAutoscroll();
    lcd.clear();
    delay(500);  
    
    lcd.clear();


}
