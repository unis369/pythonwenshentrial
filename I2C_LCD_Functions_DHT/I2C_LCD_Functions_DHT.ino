/*
    LCD_I2C - Arduino library to control a 16x2 LCD via an I2C adapter based on PCF8574

    Copyright(C) 2020 Blackhack <davidaristi.0504@gmail.com>

    This program is free software : you can redistribute it and /or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.If not, see < https://www.gnu.org/licenses/>.
*/

#include <LCD_I2C.h>

LCD_I2C lcd(0x27); // Default address of most PCF8574 modules, change according

/*
* When using lcd.print() (and almost everywhere you use string literals),
* is a good idea to use the macro F(String literal).
* This tells the compiler to store the string array in the flash memory
* instead of the ram memory. Usually you have more spare flash than ram.
* More info: https://www.arduino.cc/reference/en/language/variables/utilities/progmem/
*/

#include "DHT.h"
// set the DHT Pin
#define DHTPIN 8
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);


void setup()
{
    lcd.begin();
    lcd.backlight();
}

void loop()
{
   
    dht.begin();
  // read humidity
  float h = dht.readHumidity();
  //read temperature in Fahrenheit
  float f = dht.readTemperature(true);
  //read temperature in Celsius
  float c = dht.readTemperature();
  lcd.setCursor(6, 0);
  lcd.print("% Humidity");
  lcd.setCursor(1, 0);
  lcd.print(h);
  delay(2500);
   for (int i = 0; i < 11; i++)
    {
        lcd.scrollDisplayRight();
        delay(300);
    }
     for (int i = 0; i < 11; i++)
    {
        lcd.scrollDisplayLeft();
        delay(300);
    } 
  delay(1000);
  lcd.setCursor(6, 1);
  lcd.print("F");
  lcd.setCursor(1, 1);
  lcd.print(f);
  delay(1000);
  lcd.setCursor(13, 1);
  lcd.print("C");
  lcd.setCursor(8, 1);
  lcd.print(c);
  delay(5000);
   
    lcd.noAutoscroll();
    lcd.clear();
    delay(500);  
    // Scroll left and right
    //lcd.setCursor(10, 0);
    //lcd.print(F("To the left!"));
    //for (int i = 0; i < 10; i++)
    //{
    //    lcd.scrollDisplayLeft();
    //    delay(200);
    //}
    //lcd.clear();
    //lcd.print(F("To the right!"));
    //for (int i = 0; i < 10; i++)
    //{
    //    lcd.scrollDisplayRight();
    //    delay(200);
    //}
    lcd.clear();

    //Cursor
    //lcd.setCursor(0, 0);
    //lcd.cursor();
    //lcd.print(F("Cursor"));
    //delay(3000);
    //lcd.clear();

    //Cursor blink
    //lcd.setCursor(0, 0);
    //lcd.blink();
    //lcd.print(F("Cursor blink"));
    //delay(3000);
    //lcd.clear();

    //Blink without cursor
    //lcd.setCursor(0, 0);
    //lcd.noCursor();
    //lcd.print(F("Just blink"));
    //delay(3000);
    //lcd.noBlink();
    //lcd.clear();
}
