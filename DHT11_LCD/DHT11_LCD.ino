
#include <G:\Root_document\Arduino\libraries\LiquidCrystal\src\LiquidCrystal.h>  //Include LCD library
#include "G:\Root_document\Arduino\libraries\DHT_sensor_library\DHT.h"            //Include Library for DHT sensors
#define DHTPIN 2            //defines reader pin for the sensor as Arduino pin 8 讀取DHT11 Data
#define DHTTYPE DHT11       //set which DHT sensor is being used 選用DHT11  
DHT  dht(DHTPIN, DHTTYPE);  // Initialize DHT sensor

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

void setup() {
  
  Serial.begin(9600); //設定鮑率9600   
  dht.begin();
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Oliver's tank!");
}

void loop() {
   delay(1000);            //set rate for screen refresh can't be lower than 1000ms
   float H = dht.readHumidity();        ;//讀取濕度
   float T = dht.readTemperature()      ;//讀取攝氏溫度
   float F = dht.readTemperature(true)  ;//讀取華氏溫度
    if (isnan(H) || isnan(T) || isnan(F)) {
    Serial.println("Error , Can read from DHT sensor！");
    return;
  }
   lcd.setCursor(0, 1);
   lcd.print("     %");
   lcd.setCursor(0, 1);
   lcd.print(H);
   lcd.setCursor(8, 1);
   lcd.print("     deg");
   lcd.setCursor(8, 1);
   lcd.print(F);
  Serial.print(" 濕度: ");
  Serial.print(H);
  Serial.print("%\t");
  Serial.print(" 攝氏溫度: ");
  Serial.print(T);
  Serial.print("°C\t");
  Serial.print(" 華氏溫度: ");
  Serial.print(F);
  Serial.print("°F\n");
  delay(5000);//延時5秒
}
  
